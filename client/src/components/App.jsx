import React, {useEffect} from 'react';
import Navbar from "./navbar/Navbar";
import './app.scss'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Registration from "./authorzation/Registration";
import Login from "./authorzation/Login";
import {useDispatch, useSelector} from "react-redux";
import {auth} from "../actions/user";
import Disk from "./disk/Disk";

function App() {
    const isAuth = useSelector(state => state.user.isAuth)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(auth())
    }, [])


    return (
      <BrowserRouter>
          <div className="app">
              <Navbar />
              <div className="wrap">
                  {!isAuth ?
                      <Routes>
                          <Route path="/registration" element={<Registration />}/>
                          <Route path="/login" element={<Login />}/>
                          <Route path="*" element={<Login />} />
                      </Routes>
                      :
                      <Routes>
                          <Route path="/" element={<Disk />}/>
                      </Routes>
                  }
              </div>
          </div>
      </BrowserRouter>
  );
}

export default App;
