import React, {useState} from 'react';
import './authorization.scss'
import Input from "../../utils/input/Input";
import {registration} from "../../actions/user";

const Registration = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    return (
        <div className="authorization">
            <div className="authorization__header">Регистрация</div>
            <Input type="text" setValue={setEmail} placeholder="Введите email..." value={email}/>
            <Input type="password" setValue={setPassword}  placeholder="Введите пароль..." value={password}/>
            <button className="authorization__btn" onClick={() => registration(email, password)}>Зарегистрироваться</button>
        </div>
    );
};

export default Registration;
